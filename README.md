# Spring Boot User Test :: Sbu-test

# Task
Create SpringBoot application using SpringData and MySQL with the following endpoints:

- show all users

- find one user by id

- find users by name (might be several with the same name)

- delete user

- update user

Cover code with tests (not only unit tests), use H2 for db layer testing.

User domain class may contain the following properties: id, firstName, lastName, age, email, phone, city etc

Please actively use Java-8 features where appropriate.

### MySQL

Host: localhost

Port: 3306

Schema: sbu-test

User: juser

Password: jpass

### Test

Will use embedded H2 database

```sh
$ mvn test
```

### Running the application
The plugin includes a run goal which can be used to launch your application from the command line:

```sh
$ mvn spring-boot:run
```

### Port

8080

### Endpoints

- show all users
```sh
GET /api/user 
```	
- find one user by Id, return User
```sh
GET /api/user/{userId} 
```	    
- find users by first or last name, return list of User
```sh
GET /api/user/find
```
- delete user		
```sh
DELETE /api/user/{userId}
```
- update user
```sh
PUT /api/user/{userId}
```