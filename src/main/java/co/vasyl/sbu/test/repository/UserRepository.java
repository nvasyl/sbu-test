package co.vasyl.sbu.test.repository;

import co.vasyl.sbu.test.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findById(Integer id);

    Optional<User> findByLastName(String lastName);

    @Async
    CompletableFuture<List<User>> findByFirstNameIgnoreCaseContaining(String firstName);

    @Async
    CompletableFuture<List<User>> findByLastNameIgnoreCaseContaining(String lastName);
}
