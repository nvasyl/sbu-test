package co.vasyl.sbu.test.web;

import co.vasyl.sbu.test.domain.User;
import co.vasyl.sbu.test.service.UserService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/user")
    public List<User> showAllUsers() {
        return userService.getUsers();
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity findOneUserById(@PathVariable Integer userId) {
        return userService.getUser(userId)
                .map(user -> {return ResponseEntity.ok(user);})
                .orElse(ResponseEntity.noContent().build());
    }

    @GetMapping("/user/find")
    public ResponseEntity findUsersByName(@RequestParam(value = "name") String name) {
        try {
            return ResponseEntity.ok(userService.getUsers(name));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/user/{userId}")
    public ResponseEntity deleteUser(@PathVariable(value = "userId") Integer userId) {
        try {
            userService.delete(userId);
        } catch (NotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping("/user/{userId}")
    public ResponseEntity updateUser(@RequestBody User user, @PathVariable("userId") Integer userId) {
        return ResponseEntity.ok(userService.update(user, userId));
    }
}
