package co.vasyl.sbu.test.service;

import co.vasyl.sbu.test.domain.User;
import javassist.NotFoundException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

public interface UserService {

    Optional<User> getUser(Integer id);

    List<User> getUsers();

    List<User> getUsers(String name) throws ExecutionException, InterruptedException;

    void delete(Integer id) throws NotFoundException;

    User update(User user, Integer userId);

    User save(User user);
}
