package co.vasyl.sbu.test.service;

import co.vasyl.sbu.test.domain.User;
import co.vasyl.sbu.test.repository.UserRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Optional<User> getUser(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getUsers(String name) throws ExecutionException, InterruptedException {

        CompletableFuture<List<User>> userListFirstName = userRepository.findByFirstNameIgnoreCaseContaining(name);
        CompletableFuture<List<User>> userListLastName = userRepository.findByLastNameIgnoreCaseContaining(name);

        CompletableFuture.allOf(userListFirstName, userListLastName).join();

        List<User> userList = userListFirstName.get();
        userList.addAll(userListLastName.get());

        return userList.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public void delete(Integer id) throws NotFoundException {
        try {
            userRepository.delete(id);
        } catch (EmptyResultDataAccessException ex) {
            throw new NotFoundException(ex.getMessage());
        }
    }

    @Override
    public User update(User user, Integer userId) {
        return userRepository.save(user);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }
}
