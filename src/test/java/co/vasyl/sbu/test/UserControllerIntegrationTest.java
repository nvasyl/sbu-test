package co.vasyl.sbu.test;

import co.vasyl.sbu.test.domain.User;
import co.vasyl.sbu.test.service.UserService;
import co.vasyl.sbu.test.web.UserController;
import com.google.gson.Gson;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private User user;
    private String userJson;

    private List<User> userList;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setId(1);
        user.setFirstName("Peter");
        user.setLastName("Pen");
        user.setEmail("peter.peter@gmail.com");

        Gson gson = new Gson();
        userJson = gson.toJson(user);

        User userSec = new User();
        userSec.setId(2);
        userSec.setFirstName("Ivan");
        userSec.setLastName("Pentalgin");
        userSec.setEmail("ivan.ss@gmail.com");

        User userTh = new User();
        userTh.setId(3);
        userTh.setFirstName("Proof");
        userTh.setLastName("Link");
        userTh.setEmail("link@rt.com");

        userList = new ArrayList<>(4);
        userList.add(user);
        userList.add(userSec);
        userList.add(userTh);
    }

    @Test
    public void shouldReturnAllUsers() throws Exception {

        given(userService.getUsers()).willReturn(userList);

        mockMvc.perform(get("/api/user"))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].lastName", is("Pen")))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].firstName", is("Ivan")))

                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldReturnUserById() throws Exception {

        given(userService.getUser(anyInt())).willReturn(Optional.of(user));

        mockMvc.perform(get("/api/user/1"))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))

                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.lastName", is("Pen")))

                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldNotReturnUserById() throws Exception {

        given(userService.getUser(anyInt())).willReturn(Optional.empty());

        mockMvc.perform(get("/api/user/7"))

                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldFindUserByName() throws Exception {

        userList.remove(2);
        given(userService.getUsers(anyString())).willReturn(userList);

        mockMvc.perform(get("/api/user/find?name=any"))

                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].lastName", is("Pen")))

                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].firstName", is("Ivan")))

                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldNotDeleteUserById() throws Exception {

        doThrow(NotFoundException.class).when(userService).delete(anyInt());

        mockMvc.perform(delete("/api/user/7"))

                .andExpect(status().isNoContent());
    }

    @Test
    public void shouldDeleteUserById() throws Exception {

        doNothing().when(userService).delete(anyInt());

        mockMvc.perform(delete("/api/user/1"))

                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldUpdateUserById() throws Exception {

        given(userService.update(anyObject(), anyInt())).willReturn(user);

        mockMvc.perform(put("/api/user/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(userJson))
                .andExpect(status().is2xxSuccessful());
    }
}
