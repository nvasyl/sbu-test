package co.vasyl.sbu.test;

import co.vasyl.sbu.test.domain.User;
import co.vasyl.sbu.test.repository.UserRepository;
import co.vasyl.sbu.test.service.UserService;
import co.vasyl.sbu.test.service.UserServiceImpl;
import javassist.NotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;

public class UserServiceTest {

    @InjectMocks
    UserService userService = new UserServiceImpl();

    @Mock
    UserRepository mockUserRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetUsers_returnNoDuplicates() {

        User user = new User();
        user.setFirstName("Peter");
        user.setLastName("Pen");
        user.setEmail("peter.peter@gmail.com");

        List<User> initList = new ArrayList<>(1);
        initList.add(user);

        final CompletableFuture<List<User>> future = new CompletableFuture<>();
        future.complete(initList);

        Mockito.when(mockUserRepository.findByFirstNameIgnoreCaseContaining(anyString())).thenReturn(future);
        Mockito.when(mockUserRepository.findByLastNameIgnoreCaseContaining(anyString())).thenReturn(future);

        try {
            List<User> result = userService.getUsers(anyString());

            assertEquals(result.size(), 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(expected = NotFoundException.class)
    public void testDeleteNotExist_throwException() throws NotFoundException {
        doThrow(new EmptyResultDataAccessException(1)).when(mockUserRepository).delete(anyInt());
        userService.delete(1);
    }

    @Test
    public void testDelete_returnNothing() throws NotFoundException {
        doNothing().when(mockUserRepository).delete(anyInt());
        userService.delete(1);
    }
}
