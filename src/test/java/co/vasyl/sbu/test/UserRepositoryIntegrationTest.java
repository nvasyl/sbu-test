package co.vasyl.sbu.test;

import co.vasyl.sbu.test.domain.User;
import co.vasyl.sbu.test.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.*;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testFindUsersByName() throws Exception {

        String name = "nikolai";
        CompletableFuture<List<User>> userListFirstName = userRepository.findByFirstNameIgnoreCaseContaining(name);
        CompletableFuture<List<User>> userListLastName = userRepository.findByLastNameIgnoreCaseContaining(name);

        assertEquals(2, userListFirstName.get().size());
        assertEquals(2, userListLastName.get().size());
    }


    @Test(expected = EmptyResultDataAccessException.class)
    public void testDeleteUser() throws Exception {
        userRepository.delete(13);
    }

    @Test
    public void testFindById_returnOptional() throws Exception {
        assertFalse(userRepository.findById(13).isPresent());
        assertTrue(userRepository.findById(1).isPresent());
    }

    @Test
    public void shouldSaveAndFetchUser() throws Exception {
        User newUser = new User();
        newUser.setFirstName("Peter");
        newUser.setLastName("Pen");
        newUser.setEmail("peter.peter@gmail.com");

        userRepository.save(newUser);

        Optional<User> posiblyNewUser = userRepository.findByLastName("Pen");

        assertThat(posiblyNewUser, is(Optional.of(newUser)));
    }

    @Test
    public void shouldUpdateAndFetchUser() throws Exception {
        List<User> listUser = userRepository.findAll();
        User fisrtUser = listUser.get(0);
        fisrtUser.setLastName("Emanuil");
        userRepository.save(fisrtUser);

        Optional<User> posiblySameUser = userRepository.findByLastName("Emanuil");

        assertThat(posiblySameUser, is(Optional.of(fisrtUser)));
    }
}
